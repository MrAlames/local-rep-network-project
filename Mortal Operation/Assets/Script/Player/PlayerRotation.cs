﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
    [SerializeField]
    PhotonView myPV;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (myPV.IsMine)
        {
            Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Physics.Raycast(mouseRay, out hit, 1000f, 1 << 9);
            Vector3 lookPos = hit.point;
            lookPos.y = this.transform.position.y;
            this.transform.LookAt(lookPos);
        }
        
    }
}
