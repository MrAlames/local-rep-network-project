﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExitGames.Client.Photon;
using Photon.Pun;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour, IOnEventCallback, IPunObservable
{
    FollowCamera myCamera;

    [SerializeField]
    float hp;
    float maxHP;

    [SerializeField]
    PhotonView myPV;

    Slider myHealthBar;


    [SerializeField]
    GameObject myMeshParent;

    [SerializeField]
    Collider myCol;


    public Slider MyHealthBar { set => myHealthBar = value; }
    public FollowCamera MyCamera { set => myCamera = value; }
    public PhotonView MyPV { get => myPV; }


    private void Awake()
    {
        maxHP = hp;
    }

    private void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);

    }

    private void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);

        object[] data = new object[] {myPV.Owner};
        RaiseEventOptions reo = new RaiseEventOptions { Receivers = ReceiverGroup.Others };
        PhotonNetwork.RaiseEvent(RaiseEventCodes.PlayerLeft, data, reo, SendOptions.SendReliable);

    }

    public void TakeDamage(float damageAmount, Player killer)
    {
        if(damageAmount >= hp)
        {
            hp = 0;
            myHealthBar.value = hp;
            PlayerDead(killer);
        }
        else
        {
            hp -= damageAmount;
            myHealthBar.value = hp;
        }
    }

    public void GetHealed(float healAmount)
    {
        if(healAmount+hp >= maxHP)
        {
            hp = maxHP;
            myHealthBar.value = hp;
        }
        else
        {
            hp += healAmount;
            myHealthBar.value = hp;
        }
    }

    private void PlayerDead(Player killer)
    {
        //adjust followCamera to be in spectator mode
        myCamera.SpectatorModeOn = true;
        myPV.RPC("RemoteDead", RpcTarget.All);

        object[] data = new object[] { myPV.Owner, killer };
        RaiseEventOptions reo = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(RaiseEventCodes.PlayerDeath, data, reo, SendOptions.SendReliable);
    }

    /// <summary>
    /// Event which informs the MatchController that this player has died
    /// </summary>
    //private void SendPlayerDeath(Player killer)
    //{
    //    object[] data = new object[] { myPV.Owner, killer };
    //    RaiseEventOptions reo = new RaiseEventOptions { Receivers = ReceiverGroup.All };
    //    PhotonNetwork.RaiseEvent(RaiseEventCodes.PlayerDeath, data, reo, SendOptions.SendReliable);
    //}


    [PunRPC]
    private void RemoteDead()
    {
        myCol.enabled = false;
        myMeshParent.SetActive(false);
    }

    //public void ResetPlayer(Vector3 spawnPosition)
    //{
    //    myPV.RPC("RemoteResetPlayer", myPV.Owner, spawnPosition);
    //}

    //[PunRPC]
    public void RemoteResetPlayer(Vector3 spawnPosition)
    {
        myCol.enabled = true;
        myMeshParent.SetActive(true);


        transform.position = spawnPosition;

        hp = maxHP;

        if (myPV.IsMine)
        {
            myHealthBar.value = hp;

            myCamera.transform.position = this.transform.position + myCamera.DistanceToPlayer;
            myCamera.SpectatorModeOn = false;
        }

        //adjust myWeapons
    }

    public void OnEvent(EventData photonEvent)
    {
        byte raiseEventCode = photonEvent.Code;


        if(raiseEventCode == RaiseEventCodes.PlayerHitInfo && myPV.IsMine)
        {

            object[] data = (object[]) photonEvent.CustomData;
            if((Player)data[1] == PhotonNetwork.LocalPlayer)
            {
                TakeDamage((float)data[2],(Player)data[0]);
            }
        }

        if(raiseEventCode == RaiseEventCodes.PlayerRespawn)
        {
            object[] data = (object[])photonEvent.CustomData;
            if ((Player)data[0] == myPV.Owner)
            {
                RemoteResetPlayer((Vector3)data[1]);
            }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(hp);
        }
        else
        {
            hp = (float) stream.ReceiveNext();
        }
    }
}
