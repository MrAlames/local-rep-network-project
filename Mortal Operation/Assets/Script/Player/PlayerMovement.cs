﻿using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class PlayerMovement : MonoBehaviour, IOnEventCallback
{
    [SerializeField]
    Rigidbody myRB;
    [SerializeField]
    float speed;
    [SerializeField]
    Animator myAnimator;
    [SerializeField]
    PhotonView myPV;


    KeyCode upKey = KeyCode.W, downKey = KeyCode.S, rightKey = KeyCode.D, leftKey = KeyCode.A;

    bool hasControl = true;



    private void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);

    }

    private void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);

    }

    public void OnEvent(EventData photonEvent)
    {
        byte raiseEventCode = photonEvent.Code;

        if (raiseEventCode == RaiseEventCodes.LooseControl && myPV.IsMine)
        {

            object[] data = (object[])photonEvent.CustomData;
            if ((Player)data[0] == myPV.Owner)
            {
                hasControl = false;
            }
        }

        if (raiseEventCode == RaiseEventCodes.GainControl && myPV.IsMine)
        {

            object[] data = (object[])photonEvent.CustomData;
            if ((Player)data[0] == myPV.Owner)
            {
                hasControl = true;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (myPV.IsMine)
        {
            if (hasControl)
            {
                Vector3 directionForce;

                if (Input.GetAxis("Horizontal") >= 0.5f && Input.GetAxis("Vertical") >= 0.5f ||
                    Input.GetAxis("Horizontal") <= -0.5f && Input.GetAxis("Vertical") <= -0.5f ||
                    Input.GetAxis("Horizontal") >= 0.5f && Input.GetAxis("Vertical") <= -0.5f ||
                    Input.GetAxis("Horizontal") <= -0.5f && Input.GetAxis("Vertical") >= 0.5f)
                {
                    directionForce = new Vector3(Input.GetAxis("Horizontal") * Time.deltaTime * speed * 0.8f, 0, Input.GetAxis("Vertical") * Time.deltaTime * speed * 0.8f);
                }
                else
                {
                    directionForce = new Vector3(Input.GetAxis("Horizontal") * Time.deltaTime * speed, 0, Input.GetAxis("Vertical") * Time.deltaTime * speed);
                }

                myRB.velocity = new Vector3(0,myRB.velocity.y,0);

                myRB.AddForce(directionForce);

                myAnimator.SetFloat("Speed", directionForce.sqrMagnitude);


            }

        }
    }
}
