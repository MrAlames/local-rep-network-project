﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUps : MonoBehaviour
{
    [SerializeField]
    protected float delayTime;
    //Time until Weapon is ready to be picked up again

    [SerializeField]
    protected PhotonView mypv;

    [SerializeField]
    protected WType pickUp;

    
    protected float delayTimer;

    [SerializeField]
    protected Renderer myRenderer;

    private void Start()
    {
        mypv.RPC("SetDelay", RpcTarget.All);
    }

    void Update()
    {
        delayTimer += Time.deltaTime;

        //ends the delay if delay is set and the delay is over
        if (!myRenderer.enabled && delayTimer > delayTime)
        {
            //ending delay by making Object visible again
            myRenderer.enabled = true;
        }
    }

    protected void OnTriggerEnter(Collider col)
    {
        // only accessible if not on delay and a player collides
        if (myRenderer.enabled && col.gameObject.tag == "Player")
        {
            WeaponController myController= col.gameObject.GetComponentInChildren<WeaponController>();

            if (pickUp==WType.SMG)
            {
                myController.PickUpSMG();
            }

            if (pickUp == WType.Sniper)
            {
                myController.PickUpSniper();
            }
            ////Add weapon if its already in the list remove first
            //foreach (Weapon w in gunList)
            //{
            //    if (w.MyType == pickUp.MyType)
            //    {
            //        gunList.Remove(w);
            //    }
            //}
            //gunList.Add(pickUp);
            //Debug.Log($"picked up new {pickUp.MyType}");


        }
        //set the delay for the interaction
        mypv.RPC("SetDelay",RpcTarget.All);
    }

    [PunRPC]
    protected virtual void SetDelay()
    {
        //reset timer and disable visibility
        delayTimer = 0f;
        myRenderer.enabled = false;
    }

}
