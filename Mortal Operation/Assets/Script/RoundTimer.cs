﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RoundTimer : MonoBehaviour, IOnEventCallback
{

    [SerializeField]
    TextMeshProUGUI myTimerText;

    public void OnEvent(EventData photonEvent)
    {
        byte raiseEventCode = photonEvent.Code;
        if (raiseEventCode == RaiseEventCodes.TimerChange)
        {
            object[] data = (object[])photonEvent.CustomData;
            myTimerText.text = (string)data[0];
        }
    }

    private void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);

    }

    private void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);

    }
}
