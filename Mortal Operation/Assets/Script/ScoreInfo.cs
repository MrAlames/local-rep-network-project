﻿using System.Collections;
using System.Collections.Generic;

public class ScoreInfo
{
    string username;
    int numberKills, numberDeaths, numberWins;

    public ScoreInfo(string username, int numberKills, int numberDeaths, int numberWins)
    {
        this.username = username;
        this.numberKills = numberKills;
        this.numberDeaths = numberDeaths;
        this.numberWins = numberWins;
    }

    public string Username { get => username; set => username = value; }
    public int NumberKills { get => numberKills; set => numberKills = value; }
    public int NumberDeaths { get => numberDeaths; set => numberDeaths = value; }
    public int NumberWins { get => numberWins; set => numberWins = value; }
}
