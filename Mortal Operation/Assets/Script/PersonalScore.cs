﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExitGames.Client.Photon;
using Photon.Realtime;

public class PersonalScore : MonoBehaviour, IOnEventCallback
{

    //int roundsWon = 0;
    //int numberKills = 0;
    //int numberDeaths = 0;

    List<ScoreInfo> allScore = new List<ScoreInfo>();

    // Start is called before the first frame update
    private void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    private void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public void OnEvent(EventData photonEvent)
    {
        byte raiseEventCode = photonEvent.Code;

        if (raiseEventCode == RaiseEventCodes.RoundWin)
        {
            
            object[] data = (object[])photonEvent.CustomData;
            Player winningPlayer = (Player)data[0];
            foreach(ScoreInfo currentPlayerScore in allScore)
            {
                if (currentPlayerScore.Username == winningPlayer.NickName)
                {
                    currentPlayerScore.NumberWins++;
                }
            }
        }
        if(raiseEventCode == RaiseEventCodes.PlayerDeath)
        {
            object[] data = (object[])photonEvent.CustomData;
            Player killer = (Player)data[1];
            bool killerInList = false;
            foreach (ScoreInfo currentPlayerScore in allScore)
            {
                if (currentPlayerScore.Username == killer.NickName)
                {
                    currentPlayerScore.NumberKills++;
                    killerInList = true;
                }
            }
            if (!killerInList)
            {
                allScore.Add(new ScoreInfo(killer.NickName, 1, 0, 0));
            }
            bool deadInList = false;
            Player deadPlayer = (Player)data[0];
            foreach (ScoreInfo currentPlayerScore in allScore)
            {
                if (currentPlayerScore.Username == deadPlayer.NickName)
                {
                    currentPlayerScore.NumberDeaths++;
                    deadInList = true;
                }
            }
            if (!deadInList)
            {
                allScore.Add(new ScoreInfo(deadPlayer.NickName, 0, 1, 0));
            }
        }
    }
    //private void Update()
    //{
    //    Debug.Log($"Player: {PhotonNetwork.LocalPlayer} has {numberKills} kills and {numberDeaths} deaths. They won {roundsWon} rounds");
    //}

}
