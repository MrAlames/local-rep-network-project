﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Weapon
{
    
    void Start()
    {
        myType = WType.Pistol;
        shotFrequency = 0.5f;
        maxOffset = 0.3f;
        offsetMultiplier = 1;
        ammo = 1;
        
    }

    
    void Update()
    {  
        shotTimer += Time.deltaTime;
       
    }

    public override void Shoot()
    {
        if (CheckTimer(shotTimer) == true)
        {
            SpawnBullet(bulletPrefab, bulletSpawn);
            shotTimer = 0;
        }
    }

}
