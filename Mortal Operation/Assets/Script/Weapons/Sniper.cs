﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sniper : Weapon
{

    void Start()
    {
        myType = WType.Sniper;
        shotFrequency = 0.8f;
        maxOffset = 0f;
        offsetMultiplier = 0;
        startingAmmo = 15;
    }


    void Update()
    {
        shotTimer += Time.deltaTime;
    }


}

