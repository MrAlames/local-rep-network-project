﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMG : Weapon
{
 
    
    void Start()
    {
        myType = WType.SMG;
        shotFrequency = 0.1f;
        maxOffset = 0.3f;
        offsetMultiplier = 1;
        startingAmmo = 50;
    }


    void Update()
    {
        shotTimer += Time.deltaTime;
    }


}

