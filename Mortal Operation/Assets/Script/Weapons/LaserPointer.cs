﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPointer : MonoBehaviour
{
    private LineRenderer myLR;
    [SerializeField]
    public LayerMask bulletLayer ;
    [SerializeField]
    float length;
    void Start()
    {
        myLR = GetComponent<LineRenderer>();

    }

   
    void Update()
    {
        myLR.SetPosition(0, transform.position);
        RaycastHit hit;
        if(Physics.Raycast(transform.position,transform.forward,out hit,length,~ bulletLayer))
        {
            if (hit.collider)
            {
                myLR.SetPosition(1, hit.point);
                Debug.Log(hit.point);
            }
        }
        else
        {  
            myLR.SetPosition(1, this.transform.position+transform.forward*length);
        }
    }
}
