﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using ExitGames.Client.Photon;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    public float bulletSpeed;
    [SerializeField]
    public float bulletDamage;

    [SerializeField]
    PhotonView myPV;

    public Rigidbody myRB;

    public Vector3 offset;

    void Awake()
    {
        myRB = this.GetComponent<Rigidbody>();
    }

    private void Start()
    {
        myRB.AddForce((transform.forward+offset)*bulletSpeed);
        Destroy(gameObject,10f);
    }

    private void SendPlayerHitInfo(Player hitPlayer)
    {
        object[] data = new object[] {myPV.Owner, hitPlayer, bulletDamage};
        RaiseEventOptions reo = new RaiseEventOptions { Receivers = ReceiverGroup.Others };
        PhotonNetwork.RaiseEvent(RaiseEventCodes.PlayerHitInfo, data, reo, SendOptions.SendReliable);
    }

    private void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            Player otherPlayer = col.gameObject.GetComponent<PhotonView>().Owner;
            if (otherPlayer != myPV.Owner)
            {
                SendPlayerHitInfo(otherPlayer);
            }
        }

        Destroy(this.gameObject);
    }
}
