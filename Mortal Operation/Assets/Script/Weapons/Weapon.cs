﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public enum WType
{
    Pistol,
    SMG,
    Sniper
}

public class Weapon : MonoBehaviour
{
    #region Variables
    [SerializeField]
    public string bulletPrefab;

    protected float shotFrequency, shotTimer, offsetMultiplier,maxOffset,ammo;

   [SerializeField]
    protected float startingAmmo;

    protected WType myType;

    [SerializeField]
    protected Transform bulletSpawn;

    [SerializeField]
    Transform bulletParent;
    
    public Vector3 offset;
    

    public float OffsetMultiplier { get => offsetMultiplier; set => offsetMultiplier = value; }
    public float MaxOffset { get => maxOffset; set => maxOffset = value; }
    public WType MyType { get => myType; set => myType = value; }
    public float Ammo { get => ammo; set => ammo = value; }
    public float StartingAmmo { get => startingAmmo; set => startingAmmo = value; }

    #endregion



    public virtual void Shoot()
    {
        if (CheckTimer(shotTimer) == true && ammo > 0)
        {
            SpawnBullet(bulletPrefab, bulletSpawn);
            ammo -= 1;
            shotTimer = 0;
        }


    }

    #region Timer
   
    public virtual bool CheckTimer(float shotTimer)
    {
        if (shotTimer >= shotFrequency) return true;
        else return false;
        
    }
    #endregion

    public virtual GameObject SpawnBullet(string bulletPrefab,Transform bulletSpawn)
    {

        //GameObject bullet=GameObject.Instantiate(bulletPrefab, bulletSpawn.position, this.transform.rotation);
        GameObject bullet = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", bulletPrefab), bulletSpawn.position, this.transform.rotation);
        bullet.transform.SetParent(bulletParent);
        bullet.GetComponent<Bullet>().offset = offset;
        return bullet;
    }
}
