﻿using ExitGames.Client.Photon;
using NUnit.Framework;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WeaponController : MonoBehaviour, IOnEventCallback
{
    [SerializeField]
    List<WType> myWeapons=new List<WType>();
    [SerializeField]
    private Weapon myPistol, mySMG, mySniper;
    [SerializeField]
    PhotonView myPV;

    bool hasSniper=false, hasSMG = false;

    float offsetTimer;

    Weapon activeWeapon;

    bool hasControl = true;

    public List<WType> MyWeapons { get => myWeapons; set => myWeapons = value; }

    void Start()
    {
        activeWeapon = myPistol;
    }

    private void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);

    }

    private void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);

    }


    void Update()
    {
        if (myPV.IsMine)
        {

            if (hasControl)
            {

                if (Input.GetMouseButton(0))
                {
                    if (offsetTimer < activeWeapon.MaxOffset)
                    {
                        offsetTimer += Time.deltaTime;
                    }

                    activeWeapon.offset = new Vector3(Random.Range(-offsetTimer * activeWeapon.OffsetMultiplier, offsetTimer * activeWeapon.OffsetMultiplier), 0);
                    activeWeapon.Shoot();

                }

                if (Input.GetMouseButton(0) == false)
                {
                    if (offsetTimer > 0)
                    {
                        offsetTimer -= Time.deltaTime * 2;
                    }

                }

                if (Input.GetKeyDown(KeyCode.Alpha1))
                {
                    if (activeWeapon.MyType != WType.Pistol)
                    {
                        activeWeapon = myPistol;
                    }
                }
                if (Input.GetKeyDown(KeyCode.Alpha2) && hasSMG)
                {
                    if (activeWeapon.MyType != WType.SMG)
                    {
                        activeWeapon = mySMG;
                    }
                }
                if (Input.GetKeyDown(KeyCode.Alpha3) && hasSniper)
                {
                    if (activeWeapon.MyType != WType.Sniper)
                    {
                        activeWeapon = mySniper;
                    }
                }
            }
        }
    }

    public void PickUpSMG()
    {
        if (!hasSMG) hasSMG = true;
        mySMG.Ammo = mySMG.StartingAmmo;

    }

    public void PickUpSniper()
    {
        if (!hasSniper) hasSniper = true;
        mySniper.Ammo = mySniper.StartingAmmo;

    }

    public void ResetWeapons()
    {
        activeWeapon = myPistol;
        hasSMG = false;
        hasSniper = false;
    }

    public void OnEvent(EventData photonEvent)
    {
        byte raiseEventCode = photonEvent.Code;

        if (raiseEventCode == RaiseEventCodes.LooseControl && myPV.IsMine)
        {
            
            object[] data = (object[])photonEvent.CustomData;
            if ((Player)data[0] == myPV.Owner)
            {
                hasControl = false;
            }
        }

        if (raiseEventCode == RaiseEventCodes.GainControl && myPV.IsMine)
        {

            object[] data = (object[])photonEvent.CustomData;
            if ((Player)data[0] == myPV.Owner)
            {
                hasControl = true;
            }
        }
        if (raiseEventCode == RaiseEventCodes.PlayerRespawn)
        {
            object[] data = (object[])photonEvent.CustomData;
            if ((Player)data[0] == myPV.Owner)
            {
                ResetWeapons();
            }
        }
    }
}
