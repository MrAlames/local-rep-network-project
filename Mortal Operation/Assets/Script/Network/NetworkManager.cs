﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;
using ExitGames.Client.Photon;
using TMPro;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    [SerializeField]
    Button StartButton;
    [SerializeField]
    byte MaxPlayers;
    [SerializeField]
    GameObject matchControllerPrefab;

    [SerializeField]
    TMP_InputField usernameInput;

    bool levelLoaded = false;
    bool hostedMatch = false;

    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.ConnectUsingSettings();
        //smaller resolution for easier debugging
        Screen.SetResolution(1280, 720, false);
    }

    // Update is called once per frame
    void Update()
    {
        if(!levelLoaded)
            StartButton.interactable = PhotonNetwork.IsConnectedAndReady;

        //if (Input.GetKeyDown(KeyCode.E))
        //{
        //    object[] data = new object[] { };
        //    RaiseEventOptions reo = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        //    PhotonNetwork.RaiseEvent(RaiseEventCodes.LeaveRoom, data, reo, SendOptions.SendReliable);
        //}
    }

   

    public void ConnectButton()
    {
        if (usernameInput.text != " username...")
            PhotonNetwork.LocalPlayer.NickName = usernameInput.text;
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        CreateRoom();
    }


    private void CreateRoom()
    {
        RoomOptions ro = new RoomOptions();
        ro.MaxPlayers = MaxPlayers;
        PhotonNetwork.CreateRoom("Room " + Random.Range(10000, 99999), ro);
    }

    public override void OnCreatedRoom()
    {
        hostedMatch = true;
        //DontDestroyOnLoad(this.gameObject);
        PhotonNetwork.LoadLevel(1);
        //GameObject.Instantiate(matchControllerPrefab);
        
    }

    public override void OnJoinedRoom()
    {
        
    }
}
