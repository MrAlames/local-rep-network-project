﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using UnityEngine.UI;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.SceneManagement;



public class GameNetworker : MonoBehaviour
{
    [SerializeField]
    GameObject camPrefab;

    [SerializeField]
    List<Transform> spawnpoints = new List<Transform>();

    [SerializeField]
    Vector3 camOffset, camRotation;

    [SerializeField]
    Slider playerHealthBar;


    // Start is called before the first frame update
    void Start()
    {
        int i = Random.Range(0, spawnpoints.Count);

        GameObject myPlayer = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Player"), spawnpoints[i].position, Quaternion.identity);

        PlayerData newPlayerData = myPlayer.GetComponent<PlayerData>();
        newPlayerData.MyHealthBar = playerHealthBar;

        Quaternion camQ = new Quaternion();
        camQ.eulerAngles = camRotation;

        GameObject myCam = Instantiate(camPrefab, myPlayer.transform.position + camOffset, camQ);
        FollowCamera newFollowCam = myCam.GetComponent<FollowCamera>();
        newFollowCam.Init(myPlayer);
        newPlayerData.MyCamera = newFollowCam;

        //SendPlayerJoined(newPlayerData.MyPV.Owner);

    }  
    

    private void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    private void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }


    //private void SendPlayerJoined(Player newPlayerData)
    //{
    //    object[] data = new object[] { newPlayerData };
    //    RaiseEventOptions reo = new RaiseEventOptions { Receivers = ReceiverGroup.All };
    //    PhotonNetwork.RaiseEvent(RaiseEventCodes.PlayerJoined, data, reo, SendOptions.SendReliable);
    //}

}
