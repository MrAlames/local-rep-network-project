﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExitGames.Client.Photon;

public class MatchController : MonoBehaviour, IOnEventCallback, IInRoomCallbacks
{
    #region Variables
    [SerializeField]
    int minPlayerNumber;

    [SerializeField]
    List<Vector3> allSpawnPoints = new List<Vector3>();

    List<Player> connectedPlayers = new List<Player>();

    List<Player> playersAlive = new List<Player>();

    bool roundRunning = false, roundTimerActive = false, isHost=false;

    float roundTimer = 3,roundTimerMax=3;

    #endregion

    //public void RegisterPlayer(Player toRegister)
    //{
    //    connectedPlayers.Add(toRegister);
    //}

    //public void UnregisterPlayer(Player toUnregister)
    //{
    //    connectedPlayers.Remove(toUnregister);
    //}

    private void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);

    }

    private void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    private void Awake()
    {
        if (PhotonNetwork.LocalPlayer.IsMasterClient)
        {
            isHost = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Only the master MatchController is active

        if (PhotonNetwork.LocalPlayer.IsMasterClient)
        {
            Debug.Log(connectedPlayers.Count);
            if (roundTimerActive)
            {
                //if round timer is active: check for current timer and send events to update timer
                roundTimer -= Time.deltaTime;
                if (roundTimer <= 2)
                {
                    UpdateRoundTimer("2");
                }
                if (roundTimer <= 1)
                {
                    UpdateRoundTimer("1");
                }
                if (roundTimer <= 0)
                {
                    //when timer reaches zero, send event to update timer and event for all players to regain control
                    UpdateRoundTimer("");

                    roundTimerActive = false;
                    roundTimer = 3;
                    foreach (Player p in connectedPlayers)
                    {
                        object[] data = new object[] { p };
                        RaiseEventOptions reo = new RaiseEventOptions { Receivers = ReceiverGroup.All };
                        PhotonNetwork.RaiseEvent(RaiseEventCodes.GainControl, data, reo, SendOptions.SendReliable);
                    }
                }
            }
            if (!roundRunning && connectedPlayers.Count >= minPlayerNumber)
            {
                //if enough players present, start first round
                StartRound();
            }
            if (connectedPlayers.Count >= minPlayerNumber && roundRunning && playersAlive.Count <= 1)
            {
                //if all but one player has been killed in a round
                //send win information and reset round
                object[] data = new object[] { playersAlive[0] };
                RaiseEventOptions reo = new RaiseEventOptions { Receivers = ReceiverGroup.All };
                PhotonNetwork.RaiseEvent(RaiseEventCodes.RoundWin, data, reo, SendOptions.SendReliable);
                StartRound();
            }
        }
    }

    /// <summary>
    /// Start a new round by resetting all player data and positions
    /// </summary>
    private void StartRound()
    {
        //remember all spawnpoints
        List<Vector3> freeSpawns = new List<Vector3>();

        foreach(Vector3 point in allSpawnPoints)
        {
            freeSpawns.Add(point);
        }
        //distribute players amongst all spawnpoints if there arent any free spawn points left, reuse one and add random deviation
        Vector3 currentSpawnPoint;
        foreach(Player currentPlayer in connectedPlayers)
        {
            if (freeSpawns.Count == 0)
            {

                currentSpawnPoint = allSpawnPoints[Random.Range(0, allSpawnPoints.Count-1)] + new Vector3(Random.Range(-1, 1), 0, Random.Range(-1, 1));
                SendPlayerRespawn(currentPlayer, currentSpawnPoint);
            }
            int i = Random.Range(0, freeSpawns.Count-1);
            currentSpawnPoint = freeSpawns[i];
            SendPlayerRespawn(currentPlayer, currentSpawnPoint);
            freeSpawns.Remove(freeSpawns[i]);
        }

        //remember all active players and round as running
        roundRunning = true;
        playersAlive = new List<Player>();

        foreach(Player p in connectedPlayers)
        {
            playersAlive.Add(p);

            //take away control from all players
            object[] data = new object[] { p };
            RaiseEventOptions reo = new RaiseEventOptions { Receivers = ReceiverGroup.All };
            PhotonNetwork.RaiseEvent(RaiseEventCodes.LooseControl, data, reo, SendOptions.SendReliable);
        }

        //start the roundTimer
        roundTimerActive = true;
        UpdateRoundTimer("3");
    }

    /// <summary>
    /// Sends out an event to set the text of all roundstart-timers
    /// </summary>
    /// <param name="timerText"></param>
    private void UpdateRoundTimer(string timerText)
    {
        object[] data = new object[] { timerText };
        RaiseEventOptions reo = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(RaiseEventCodes.TimerChange, data, reo, SendOptions.SendReliable);
    }

    /// <summary>
    /// Send out an event to respawn a player at a certain point
    /// </summary>
    /// <param name="respawningPlayer"></param>
    /// <param name="spawnPoint"></param>
    private void SendPlayerRespawn(Player respawningPlayer, Vector3 spawnPoint)
    {
        object[] data = new object[] { respawningPlayer, spawnPoint };
        RaiseEventOptions reo = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(RaiseEventCodes.PlayerRespawn, data, reo, SendOptions.SendReliable);
    }


    public void OnEvent(EventData photonEvent)
    {
       
        byte raiseEventCode = photonEvent.Code;


        //if (raiseEventCode == RaiseEventCodes.PlayerJoined)
        //{

        //    object[] data = (object[])photonEvent.CustomData;
        //    connectedPlayers.Add((Player)data[0]);

        //}

        //if(raiseEventCode == RaiseEventCodes.PlayerLeft)
        //{

        //    object[] data = (object[])photonEvent.CustomData;
        //    connectedPlayers.Remove((Player)data[0]);

        //}
        if (raiseEventCode == RaiseEventCodes.PlayerDeath)
        {
            object[] data = (object[])photonEvent.CustomData;
            playersAlive.Remove((Player)data[0]);
        }
    }

    #region IInRoomCallbacks
    public void OnPlayerEnteredRoom(Player newPlayer)
    {
        connectedPlayers.Add(newPlayer);
        if (!connectedPlayers.Contains(PhotonNetwork.LocalPlayer))
        {
            connectedPlayers.Add(PhotonNetwork.LocalPlayer);
        }
    }

    public void OnPlayerLeftRoom(Player otherPlayer)
    {
        connectedPlayers.Remove(otherPlayer);
    }

    public void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        //throw new System.NotImplementedException();
    }

    public void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        //throw new System.NotImplementedException();
    }

    public void OnMasterClientSwitched(Player newMasterClient)
    {
        //throw new System.NotImplementedException();
        if (PhotonNetwork.LocalPlayer == newMasterClient)
        {
            isHost = true;
        }
        else
        {
            isHost = false;
        }
    }
    #endregion
}
