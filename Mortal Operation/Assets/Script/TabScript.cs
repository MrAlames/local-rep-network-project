﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabScript : MonoBehaviour
{
    [SerializeField] GameObject TabMenu;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Tab))
        {
            TabMenu.gameObject.SetActive(true);
        }
        else
        {
            TabMenu.gameObject.SetActive(false);
        }

        
    }
}
