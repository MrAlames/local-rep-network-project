﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovTest : MonoBehaviour
{
    [SerializeField] float mySpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(horizontal, 0f, vertical);
        transform.position += movement * Time.deltaTime * mySpeed;
    }
}
